// For in loop
let person = {
  "first_name": "John",
  "last_name": "Doe",
  "age": 26
};

for (data in person) {
  console.log(`Attribute ${data} is: ${person[data]}`);
}

let persons = [
  {
    "name": "John Doe",
    "age": 26
  },
  {
    "name": "Andrew Foreman",
    "age": 64
  },
  {
    "name": "Sylvester",
    "age": 18
  }
];

persons.forEach(person => {
  console.log(`${person.name} is ${person.age} years old`);
});