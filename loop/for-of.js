// For of loop
let persons = [
  {
    "name": "John Doe",
    "age": 26
  },
  {
    "name": "Andrew Foreman",
    "age": 64
  },
  {
    "name": "Sylvester",
    "age": 18
  }
];

for (let person of persons) {
  console.log(person);
}

for (let char of "Hello World") {
  console.log(char);
}