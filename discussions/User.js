class User {
  constructor(Id, FirstName, LastName, YearBirth, MonthBirth, DateBirth) {
    // Properties
    this.Id = Id;
    this.FirstName = FirstName;
    this.LastName = LastName;
    this.YearBirth = YearBirth;
    this.MonthBirth = MonthBirth;
    this.DateBirth = DateBirth;
    this.isVerified = false;
  }
  // Methods
  getFullName() {
    return `${this.FirstName} ${this.LastName} `;
  }
  getBirthdayDate() {
    return `${this.YearBirth}-${this.MonthBirth}-${this.DateBirth}`;
  }
  setVerified() {
    this.isVerified = true;
  }
  setUnverified() {
    this.isVerified = false;
  }
  getVerificationStatus() {
    if (this.isVerified === true) {
      return 'This user verification status is verified';
    }
    else {
      return 'This user verification status is unverified';
    }
  }
}

// Implementation
// let Person1 = new User(
//   1,
//   "Person First",
//   "Last",
//   1976,
//   4,
//   11
// );
// Person1.setVerified();
// console.log(Person1)
// console.log(Person1.getFullName());
// console.log(Person1.getBirthdayDate());
// console.log(Person1.getVerificationStatus());

// let Person2 = new User(2, "Person 2", "Last Name", 1976, 13, 20);
// console.log(Person2);
// console.log(Person2.getVerificationStatus());

module.exports = User;