class Transaction {

  #paymentList = ["debit", "credit", "cash"];
  #payment = null;

  constructor(
    idUser,
    idProduct,
    yearTransaction,
    monthTransaction,
    dateTransaction
  ) {
    this.idUser = idUser;
    this.idProduct = idProduct;
    this.yearTransaction = yearTransaction;
    this.monthTransaction = monthTransaction;
    this.dateTransaction = dateTransaction;
    this.isPaid = false;
  }

  getUser() {
    return this.idUser;
  }

  getProduct() {
    return this.idProduct;
  }

  getTransactionDate() {
    return `${this.yearTransaction}-${this.monthTransaction}-${this.dateTransaction}`;
  }

  getPayment() {
    return this.#payment;
  }

  setPayment(payment) {
    if (this.#paymentList.indexOf(payment) !== -1) {
      this.#payment = payment;
    }
  }

  setIsPaid() {
    this.isPaid = true;
  }

  getIsPaid() {
    return this.isPaid;
  }
}

// let transactionA = new Transaction(1, 1, 2023, 12, 1);

// transactionA.setPayment("debit");
// transactionA.setIsPaid();

// console.log(transactionA.getPayment());
// console.log(transactionA.getTransactionDate());
// console.log(transactionA.getIsPaid());

module.exports = Transaction;