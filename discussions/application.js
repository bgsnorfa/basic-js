const Transaction = require("./Transaction");
const Product = require("./Product");
const User = require("./User");

let users = [];
let products = [];
let transactions = [];

console.log("App launched\nProduct list");
let product1 = new Product(1, "Sepatu sneakers", "Sepatu sneakers terbaik", 120000, "image.jpg", 5);
let product2 = new Product(2, "Baju Oversized", "Baju oversized tidak kekecilan", 50000, "image.jpg", 10);
products.push(product1, product2);

console.table(products);

console.log("Andy registered with id: 1\nUser list");
let user1 = new User(1, "Andy", "Carol", 1999, 1, 5);
user1.getVerificationStatus();
user1.setVerified();

users.push(user1);

console.table(user1);

console.log("Andy buy product 1");
let transaction1 = new Transaction(user1.Id, product1.Id, 2023, 1, 13);
transaction1.setPayment("cash");
transaction1.setIsPaid();

let transaction2 = new Transaction(user1.Id, product2.Id, 2023, 1, 14);
transaction2.setPayment("debit");
transaction2.setIsPaid();

transactions.push(transaction1, transaction2);

console.log("Transaction summary");
console.table(transactions);