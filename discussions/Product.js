class Product {
  // static isActive = true;

  constructor(
    Id,
    Name,
    Description,
    Price,
    Image,
    Discount
  ) {
    this.Id = Id;
    this.Name = Name;
    this.Description = Description;
    this.Price = Price;
    this.Image = Image;
    this.Discount = Discount;
    this.isActive = true;
  }

  getName() {
    return this.Name;
  }

  getDescription() {
    return this.Description;
  }

  getPrice() {
    return this.Price;
  }

  getPriceAfterDiscount() {
    let priceAfter = this.Price - (this.Price * this.Discount / 100);
    return priceAfter;
  }

  setInactive() {
    this.isActive = false;
  }

  setActive() {
    this.isActive = true;
  }

  getProductStatus() {
    return this.isActive;
  }

}

// let test = new Product(
//   1,
//   "makan",
//   "makanan",
//   10000,
//   "gambar",
//   10
// );

// let test2 = new Product(
//   2,
//   "jajan",
//   "jajanan",
//   20000,
//   "gambar jajan",
//   5
// );

// test.setInactive();

// console.log(
//   test.getName(),
//   test.getDescription(),
//   test.getPrice(),
//   test.getPriceAfterDiscount(),
//   test.getProductStatus()
// );

// console.log(
//   test2.getName(),
//   test2.getDescription(),
//   test2.getPrice(),
//   test2.getPriceAfterDiscount(),
//   test2.getProductStatus()
// );

module.exports = Product;