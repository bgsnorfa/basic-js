# Basic Javascript
#### This project will guide you to learn the basic of Javascript.
Ps: I suggest installing Node Version Manager (nvm)

#### This project covers:
- Data structures
- Arithmetic operations
- Object Oriented Programming (OOP) concept
- Functional Programming concept
- Document Object Model (DOM)
