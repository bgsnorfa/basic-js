class Todo {
  #tasks = [];

  getTasks() {
    return this.#tasks;
  }

  createTask(task) {
    let newTask = this.#newTaskData(task);
    this.#tasks.push(newTask);
  }

  updateTaskStatus(idx) {
    this.#tasks[idx]['done'] = !this.#tasks[idx]['done'];
  }

  #newTaskData(task) {
    return {
      "task": task,
      "done": false
    };
  }
}

function refresh() {
  let list = document.getElementById('list');
  list.innerHTML = "";
  todo.getTasks().forEach((t, idx) => {
    let data = `<li class="task ${t.done ? 'done' : ''}" data-task="${idx}" onClick="updateTask(this)">${t.task}</li>`;
    list.innerHTML += data;
  });
}

function updateTask(task) {
  let taskId = task.getAttribute('data-task');
  todo.updateTaskStatus(taskId);
  refresh();
}

let todo = new Todo();

let taskInput = document.getElementsByName('task')[0];
let addBtn = document.getElementById('addBtn');

addBtn.addEventListener('click', e => {
  e.preventDefault();

  if (taskInput.value === "") {
    taskInput.classList.add('is-invalid');
  } else {
    taskInput.classList.remove('is-invalid');
    todo.createTask(taskInput.value);
    refresh();
    taskInput.value = "";
  }
});