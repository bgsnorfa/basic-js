class Car {
  static wheelCount = 4;

  constructor(name, year, doorCount) {
    this.name = name;
    this.year = year;
    this.doorCount = doorCount;
  }

  getName() {
    return this.name;
  }

  getYear() {
    return this.year;
  }

  setName(name) {
    this.name = name;
  }

  setYear(year) {
    this.year = year;
  }
}

let audi = new Car("Audi", 1950, 4);

class ElectricCar extends Car {
  constructor(name, year, doorCount, batteryTech) {
    super(name, year, doorCount);
    this.batteryTech = batteryTech;
  }

  getBatteryTech() {
    return this.batteryTech;
  }

  setBatteryTech(batteryTech) {
    this.batteryTech = batteryTech;
  }

  getName() {
    return `${this.name} is an Electric Vehicle`;
  }
}

let audi2 = new ElectricCar("Audi EV", 2022, 2, "Lithium");
console.log(audi2);
audi2.setBatteryTech("Li-ion");
console.log(audi2.getBatteryTech());
console.log(audi2.getName(), audi2.getYear());