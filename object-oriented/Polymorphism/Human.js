class Human {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hello my name is ${this.name}`);
  }

  work() {
    console.log(`${this.constructor.name} is working`);
  }
}

// mix-ins
const PublicServer = Base => class extends Base {
  save() {
    console.log("SFX: Thank you");
  }
}

const Military = Base => class extends Base {
  shoot() {
    console.log("DOR");
  }
}

// implementation
// Doctor > PublicServer > Human
class Doctor extends PublicServer(Human) {
  constructor(name, address) {
    super(name, address);
  }

  work() {
    super.work();
    super.save();
  }
}

let doctor1 = new Doctor("dr strange", "Home");
doctor1.work();

// Police > Military > PublicServer > Human
class Police extends Military(PublicServer(Human)) {
  constructor(name, address) {
    super(name, address);
  }

  work() {
    super.work();
    super.shoot();
    super.save();
  }
}

let police1 = new Police("police 1", "Home");
police1.work();