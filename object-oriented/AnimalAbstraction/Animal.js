class Animal {

  // Properties
  #type;
  #footCount;
  #color;
  #howToGiveBirth;

  constructor() {
    if (this.constructor === Animal) {
      throw new Error("Cannot instantiate from abstract Animal");
    }
  }

  // Methods
  setType(type) {
    this.#type = type;
  }

  getType() {
    return this.#type;
  }

  setFootCount(footCount) {
    this.#footCount = footCount;
  }

  getFootCount(footCount) {
    return this.#footCount;
  }

  setColor(color) {
    this.#color = color;
  }

  getColor() {
    return this.#color;
  }
}

// mix-ins
const GivingBirth = Base => class extends Base {
  giveBirth() {
    console.log("new 🐱 born");
  }
}

const LayingEgg = Base => class extends Base {
  layEgg() {
    console.log("laying 🐣");
  }
}

// Cat -> GivingBirth -> Animal
class Cat extends GivingBirth(Animal) {
  constructor() {
    super();
  }
}

// Chicken -> LayingEgg -> Animal
class Chicken extends LayingEgg(Animal) {
  constructor() {
    super();
  }
}

// Shark -> GivingBirth -> LayingEgg -> Animal
class Shark extends GivingBirth(LayingEgg(Animal)) {
  constructor() {
    super();
  }
}

let sh1 = new Shark();

sh1.layEgg();
sh1.giveBirth();
