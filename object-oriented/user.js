class User {
  // private
  #password;
  
  constructor(email) {
    this.email = email;
  }

  getUserEmail() {
    return this.email;
  }

  setUserPassword(password) {
    this.#password = password;
  }

  login(email, password) {
    if (email === this.email && this._verifyPassword(password)) {
      return "SUCCESS";
    } else {
      return "FAIL";
    }
  }

  // protected
  _verifyPassword(password) {
    if (password === this.#password) {
      return true;
    } else {
      return false;
    }
  }
}

class Admin extends User {
  constructor(email) {
    super(email);
    this.isAdmin = true;
  }

  loginAsAdmin(email, password) {
    if (email === this.email && this._verifyPassword(password) && this.isAdmin) {
      return "SUCCESS";
    } else {
      return "FAIL";
    }
  }
}

let user1 = new User("user1@mail.com");
user1.setUserPassword("abcdef");
checkLogin = user1.login("user1@mail.com", "abcdef7");

console.log(checkLogin);

let user2 = new Admin("user2@mail.com");
user2.setUserPassword("123456");
checkLogin = user2.login("user2@mail.com", "123456");

console.log(checkLogin);

