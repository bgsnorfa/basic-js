// abstract Human
class Human {
  constructor(props) {
    if (this.constructor === Human) {
      throw new Error("Cannot instantiate from abstract Human");
    }
    let { name, address } = props;
    this.name = name;
    this.address = address;
    this.profession = this.constructor.name;
  }

  work() {
    console.log("Working");
  }

  introduce() {
    console.log(`Hello my name is ${this.name}`);
  }
}

// class Police
class Police extends Human {
  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    console.log("Go to police station");
    super.work();
  }
}

let person1 = new Police({
  name: "Person 1",
  address: "Address Person",
  rank: "⭐⭐⭐⭐⭐"
});

person1.introduce();
person1.work();
