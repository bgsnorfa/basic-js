// abstraction
class House {
  // Protected properties
  _doors;
  _windows;
  _address;
  _length;
  _width;

  constructor(props) {
    if (this.constructor === House) {
      throw new Error("Cannot create object from House abstraction");
    }
    let { doors, windows, address, length, width } = props;
    this._doors = doors;
    this._windows = windows;
    this._address = address;
    this._length= length;
    this._width = width;
  }
}

class House36 extends House {
  constructor(props) {
    super(props);
  }

  getDoors() {
    return this._doors;
  }

  getWindows() {
    return this._windows;
  }

  getAddress() {
    return this._address;
  }

  getLength() {
    return `${this._length} meters`;
  }

  getWidth() {
    return `${this._width} meters`;
  }

  getSummary() {
    return `House size: ${this._length * this._width} m²`;
  }
}

let house36 = new House36({
  doors: 2,
  windows: 4,
  address: "Street address 5",
  length: 6,
  width: 6
});

console.log(house36.getSummary());