class Person {
  // static property
  static isWalkingOn2Feet = true;
  static canBreathInWater = false;

  // instantiation
  constructor(name, age, hobbies) {
    // instance property
    this.personName = name;
    this.personAge = age;
    this.personHobbies = hobbies;
  }

  // methods
  // instance method
  talking(sentence) {
    console.log(sentence);
  }

  addAge() {
    this.personAge = this.personAge + 1;
  }

  // static method
  static blink() {
    console.log('Person is blinking');
  }
}

let andy = new Person("andy", 20, ["fishing", "gaming"]);

// Accessing instance properties
console.log(andy.personName, andy.personAge);

// Accessing static properties
// can only be accessed by class
console.log(Person.isWalkingOn2Feet, Person.canBreathInWater);

// Accessing instance methods
andy.talking("Hello, World!");
andy.addAge();
console.log(andy.personAge);

// Accessing static methods
Person.blink();