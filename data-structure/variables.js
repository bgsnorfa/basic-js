// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Creating variables
// 1. var
var a = "var";

// 2. let
let b = "let";

// 3. const
const c = "const";

console.table({
  "creating variable": {
    "var": a,
    "let": b,
    "const": c
  }
});

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Updating variable value
// 1. var
a = "var updated";

// 2. let
b = "let updated";

// 3. const (we cannot update value in const ~ const == constant)

console.table({
  "updating value in variable": {
    "var": a,
    "let": b,
    "const": "we cannot update value in const"
  }
});
