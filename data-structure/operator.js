// Operator in javascript

let a = 5;
let b = 2;

// 1. Addition
let add = a + b;

// 2. Substraction
let substract = a - b;

// 3. Multiplication
let multiplication = a * b;

// 4. Division
let divide = a / b;

// 5. Remainder
let remainder = a % b;

// 6. Exponentiation
let exponentiation = a ** b;

// 7. Pre increment
let preInc = 10;
preInc = ++preInc;

// 8. Post increment
let postInc = 10;
postInc = postInc++;

// 9. Pre Decrement
let preDec = 10;
preDec = --preDec;

// 10. Post Decrement
let postDec = 10;
postDec = postDec--;

console.table({
  "a": a,
  "b": b,
  "add": add,
  "substract": substract,
  "multiplication": multiplication,
  "divide": divide,
  "remainder": remainder,
  "exponentiation": exponentiation,
  "preInc": preInc,
  "postInc": postInc,
  "preDec": preDec,
  "postDec": postDec
});

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Comparison
console.table({
  "a": a,
  "b": b,
  "a > b": a > b,
  "a < b": a < b,
  "a == b": a == b,
  "a === b": a === b,
  "a != b": a != b,
  "a !== b": a !== b,
  "a <= b": a <= b,
  "a >= b": a >= b
});

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Logical
console.table({
  "T && F": true && false,
  "T || F": true || false,
  "!": !true
});