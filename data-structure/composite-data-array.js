// Composite data types are constructed by 
// one or multiple primitive data type
// or another composite data type

// Array
let a = [4, 10, 8];
let b = ["apple", "mango", "banana"];
let c = [1.1244, "apple", 2, "juice", true, NaN, undefined, null];

console.log("Array of Numbers");
console.table(a);

console.log("Array of strings");
console.table(b);

console.log("Array of anything");
console.table(c);

// Array operations
console.log("-=-=- Array operations -=-=-");
let fruits = b;

// array of fruits
console.table({
  "array": fruits
});

// push
fruits.push("kiwi");

console.table({
  "push(kiwi)": fruits
});

// shift
fruits.shift();

console.table({
  "shift()": fruits
});


// unshift
fruits.unshift("pineapple");

console.table({
  "unshift(pineapple)": fruits
});

// pop
fruits.pop();

console.table({
  "pop()": fruits
});

// indexOf
console.table({
  "indexOf('mango')": fruits.indexOf('banana'),
  "indexOf('orange')": fruits.indexOf('orange')
});

// length
console.table({
  "length": fruits.length
});

// Accessing & changing value
fruits[0] = "grape";
console.table({
  "fruits[0]": fruits[0],
  "fruits": fruits
});

// concatenation
new_fruits = ["dragon fruit", "mangosteen"];
fruits = fruits.concat(new_fruits);
console.table({
  "concatenation": fruits
});

// Splice
fruits.splice(2, 1, "orange");
console.table({
  "splice": fruits
});

// Slice
fruits = fruits.slice(0, 2);
console.table({
  "slice(0, 2)": fruits
});

// Sort
let months = ["jan", "feb", "mar", "apr", "may"];
months.sort().reverse();
console.table({
  "sort": months
});

// Reverse
months.sort().reverse();
console.table({
  "reverse": months
});