// Composite data types are constructed by 
// one or multiple primitive data type
// or another composite data type

// Object
let a = {};
let b = {
  "name": "John Doe",
  "email": "john.doe@email.com",
  "phone": "123-123-123",
  "hobbies": ["fishing", "football", "swimming"]
};

console.table(b);

let c = [
  {
    "type": "color",
    "value": "red"
  },
  {
    "type": "color",
    "value": "green"
  },
  {
    "type": "color",
    "value": "blue"
  }
];

console.table(c);

console.log("-=-=- Accessing object value -=-=-");
console.table({
  "first color": c[0].value,
  "second color": c[1]['value']
})

console.log("-=-=- Modifying object value -=-=-");
c[2]['value'] = "yellow";
console.table({
  "third color": c[2]['value']
});