// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// String
let a = "Hello, World!";

// Integer
let b = 1;

// Float/double
let c = 1.2;

// Boolean
let d = true;
let e = false;

// Null
let f = null;

// NaN (Not a Number)
let g = NaN;

// undefined
let h = undefined;

// String literal
let i = `${a} I have ${b} apple and $${c} in my pocket.`;

// console.table({
//   "Primitive data type": {
//     "string": a,
//     "number (integer)": b,
//     "number (float)": c,
//     "boolean (true)": d,
//     "boolean (false)": e,
//     "null": f,
//     "NaN": g,
//     "undefined": h
//   }
// });

// console.log(i);

let angka = 0;
console.log(angka == 0);