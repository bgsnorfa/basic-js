class Calculator {
  _dataString;
  
  constructor() {
    this._dataString = "";
  }

  getDataString() {
    return this._dataString;
  }

  addDataString(string) {
    this._dataString += string;
  }

  calculate() {
    return eval(this._dataString);
  }

  clear() {
    this._dataString = "";
  }
}

let data = new Calculator();

let result = document.getElementsByName('result')[0];

let num0 = document.getElementsByClassName('0')[0];
let num1 = document.getElementsByClassName('1')[0];
let num2 = document.getElementsByClassName('2')[0];
let num3 = document.getElementsByClassName('3')[0];
let num4 = document.getElementsByClassName('4')[0];
let num5 = document.getElementsByClassName('5')[0];
let num6 = document.getElementsByClassName('6')[0];
let num7 = document.getElementsByClassName('7')[0];
let num8 = document.getElementsByClassName('8')[0];
let num9 = document.getElementsByClassName('9')[0];

let add = document.getElementsByClassName('add')[0];
let subs = document.getElementsByClassName('subs')[0];
let times = document.getElementsByClassName('times')[0];
let divide = document.getElementsByClassName('divide')[0];

let calculate = document.getElementsByClassName('calculate')[0];

let clear = document.getElementsByClassName('clear')[0];

num0.addEventListener('click', e => data.addDataString("0"));
num1.addEventListener('click', e => data.addDataString("1"));
num2.addEventListener('click', e => data.addDataString("2"));
num3.addEventListener('click', e => data.addDataString("3"));
num4.addEventListener('click', e => data.addDataString("4"));
num5.addEventListener('click', e => data.addDataString("5"));
num6.addEventListener('click', e => data.addDataString("6"));
num7.addEventListener('click', e => data.addDataString("7"));
num8.addEventListener('click', e => data.addDataString("8"));
num9.addEventListener('click', e => data.addDataString("9"));

add.addEventListener('click', e => data.addDataString("+"));
subs.addEventListener('click', e => data.addDataString("-"));
times.addEventListener('click', e => data.addDataString("*"));
divide.addEventListener('click', e => data.addDataString("/"));

clear.addEventListener('click', e => {
  let result = document.getElementsByName('result')[0];
  result.value = null;
  data.clear();
});

calculate.addEventListener("click", event => {
  let calculated = data.calculate();
  let result = document.getElementsByName('result')[0];
  result.value = calculated;
  data.clear();
});

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
let email = document.getElementById('email');
let emailHelp = document.getElementById('emailHelp');
let password = document.getElementById('password');
let passwordHelp = document.getElementById('passwordHelp');

let submit = document.getElementById('submit');
let submitHelp = document.getElementById('submitHelp');

email.addEventListener('keyup', e => {
  if (email.value === '') {
    email.classList.add('is-invalid');
    emailHelp.classList.add('invalid-feedback');
    emailHelp.textContent = "Your email cannot be empty";
  } else {
    email.classList.remove('is-invalid');
    emailHelp.classList.remove('invalid-feedback');
    emailHelp.textContent = "We'll never share your email with anyone";
  }
});

password.addEventListener('keyup', e => {
  if (password.value === '') {
    password.classList.add('is-invalid');
    passwordHelp.classList.add('invalid-feedback');
    passwordHelp.textContent = "Your password cannot be empty";
  } else {
    password.classList.remove('is-invalid');
    passwordHelp.classList.remove('invalid-feedback');
    passwordHelp.textContent = "We'll never share your password with anyone";
  }
});

submit.addEventListener('click', e => {
  e.preventDefault();
  if (email.value === '' && password.value === '') {
    email.classList.add('is-invalid');
    password.classList.add('is-invalid');
    emailHelp.classList.add('invalid-feedback');
    emailHelp.textContent = "Your email cannot be empty";
    passwordHelp.classList.add('invalid-feedback');
    passwordHelp.textContent = "Your password cannot be empty";
    submitHelp.textContent = "Check your input";
    submitHelp.classList.add('invalid-feedback');
  } else {
    email.classList.remove('is-invalid');
    emailHelp.classList.remove('invalid-feedback');
    emailHelp.textContent = "We'll never share your email with anyone";
    password.classList.remove('is-invalid');
    passwordHelp.classList.remove('invalid-feedback');
    passwordHelp.textContent = "We'll never share your password with anyone";
    submitHelp.textContent = "You're ready to go";
    submitHelp.classList.remove('invalid-feedback');
  }
});