// if else in javascript
let a = true;
let b = false;
let c = 5;
let d = 20;

console.log("Single if statement");
if (a) {
  console.log(a);
}

console.log("\nIf else statement");
if (c > d) {
  console.log("Hello");
} else {
  console.log("World")
}

console.log("\nIf else if else statement");
if (c > d) {
  console.log(`${c} > ${d}`);
} else if (c < d) {
  console.log(`${c} < ${d}`);
} else {
  console.log(`${c} = ${d}`);
}

console.log("\nIf in if statement");
if (a) {
  if (!b) {
    console.log("both are true");
  } else {
    console.log("a is true, b is false");
  }
} else {
  console.log("a is false");
}