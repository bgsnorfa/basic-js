// Switch statement
let state = "morning";
switch (state) {
  case "morning":
    console.log("Breakfast");
    break;
  case "day":
    console.log("Brunch");
    break;
  case "afternoon":
    console.log("Lunch");
    break;
  case "evening":
    console.log("Dinner");
    break;
  case "night":
    console.log("Late night snack");
    break;
  default:
    console.log("Undefined time state");
    break;
}