// Function in javascript
function exponentiation(a, b) {
  return a ** b;
}

exponent = exponentiation(5, 3);
console.table({exponent});

let calculator = (a, b, operation) => {
  switch (operation) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
    case "**":
      return a ** b;
    case "%":
      return a % b;
    default:
      return NaN;
  }
}

plus = calculator(1, 4, "+");
subs = calculator(1, 4, "-");
multiply = calculator(4, 7, "*");
divide = calculator(4, 7, "/");
undef = calculator(4, 7, "&");

console.table({plus, subs, multiply, divide, undef});